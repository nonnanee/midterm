/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.midterm;

/**
 *
 * @author nonnanee
 */
public class StylsFive extends Styls{
    public StylsFive(String name, String shirt, String pants, String colorS, String colorP) {
        super(name, shirt, pants, colorS, colorP);
        System.out.println("StylsFive created");
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void pose() {
        super.pose();
        System.out.println("StyleFive pose: Stand with your hands in front.");
        System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _");
        System.out.println(" ");
    }
}
