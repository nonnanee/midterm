/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.midterm;

/**
 *
 * @author nonnanee
 */
public class StylsFour extends Styls {
    public StylsFour(String name, String shirt, String pants, String colorS, String colorP) {
        super(name, shirt, pants, colorS, colorP);
        System.out.println("StylsFour created");
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void pose() {
        super.pose();
        System.out.println("StyleFour pose: Stand with your legs slightly facing to the side, raising your arms along the way.");
        System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _");
        System.out.println(" ");
    }
}
