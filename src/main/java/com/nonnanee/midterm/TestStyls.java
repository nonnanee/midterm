/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.midterm;

/**
 *
 * @author nonnanee
 */
public class TestStyls {
    public static void main(String[] args) {
       Styls styls = new Styls("StylsZero", "LongSleeve ","LongJeans","Pink"
               ,"Black");
       styls.show();
       styls.pose();
       
       StylsOne one = new StylsOne("One","ShortSleeveT-shirt ","Shorts","Black"
               ,"Black");
       one.show();
       one.pose();
       
       StylsTwo two = new StylsTwo("Two","LongSleeve ","DenimBib","BlackLightBrown"
               ,"Blue");
       two.show();
       two.pose();
       
       StylsThree three = new StylsThree("Three","ShortSleeveT-shirt ","Shorts","Grey"
               ,"Black");
       three.show();
       three.pose();
       
       StylsFour four = new StylsFour("Four","ShortSleeveT-shirt ","ShortJeans","BlackRed"
               ,"Black");
       four.show();
       four.pose();
       
       StylsFive five = new StylsFive("Five","CropTop ","Trousers","LightBrown"
               ,"DarkGreen");
       five.show();
       five.pose();
       
       StylsSix six = new StylsSix("Six","LongSleeve ","ShortJeans","White"
               ,"Black");
       one.show();
       one.pose();
    }
}
