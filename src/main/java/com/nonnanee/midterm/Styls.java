/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.midterm;

/**
 *
 * @author nonnanee
 */
public class Styls {

    protected String name;
    protected String shirt;
    protected String pants;
    protected String colorS;
    protected String colorP;

    public Styls(String name, String shirt, String pants, String colorS,String colorP) {
        System.out.println("Styls created");
        this.name = name;
        this.shirt = shirt;
        this.pants = pants;
        this.colorS = colorS;
        this.colorP = colorP;
    }


    public void show() {
        System.out.println("Styls show");
        System.out.println("name: " + this.name +','+ "shirt: " + this.shirt
                +','+ "colorS: " + this.colorS +','+ "pants: " + this.pants
                +','+ "colorP: " + this.colorP);
        
    }

    public void pose() {
        System.out.println("Style pose");
        
    }

    public String getName() {
        return name;
    }

    public String getShirt() {
        return shirt;
    }

    public String getPants() {
        return pants;
    }

    public String getColorS() {
        return colorS;
    }

    public String getColorP() {
        return colorP;
    }

}
